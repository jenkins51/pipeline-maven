#!/bin/bash

echo "*********************"
echo "****Pushing Image****"
echo "*********************"

IMAGE="maven-project"

echo "****Logging in****"

docker login -u yashshrivastav -p $PASS
echo "****Tagging Image****"
docker tag $IMAGE:$BUILD_TAG yashshrivastav/$IMAGE:$BUILD_TAG
echo "****Pushing Image****"
docker push yashshrivastav/$IMAGE:$BUILD_TAG

